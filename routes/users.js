const express = require('express');
const controller = require('../controllers/users');
const router = express.Router();

/* GET users listing. */
router.get('/:n1/:n2', controller.sum);

router.post('/:n1/:n2', controller.mult);

router.put('/:n1/:n2', controller.div);

router.patch('/:n1/:n2', controller.pw);

router.delete('/:n1/:n2', controller.sub);

module.exports = router;
